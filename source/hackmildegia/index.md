---
title: HACKmildegia
id: 12
date: 2014-08-25 16:32:53
---

**HACKmildegia** [Txondorra](http://txondorra.net) guneko **[hacklab](https://eu.wikipedia.org/wiki/Hacklab)**-a da. **Software**, **hardware**, **elektronika**, **robotika**, ... laborategia da, guztioi irekia eta librea!



#### Hainbat ekimen burutzen ditugu:

*   **Txondorra** gunearen azpiegitura teknologikoa (webgunea, wifi sarea, ordenagailu publikoa) kudeatzea
*   [**Ttan Ttakun**](http://ttanttakun.org) irratiaren ikerketa eta garapen laborategia
*   [**Kabina Kolektiboak**](http://hackmildegia.net/kabina-kolektiboak/): kolektiboentzako internet zerbitzu autogestionatuak*   Tailer eta ikastaroak
*   Software libre proiektuak laguntzea: garapena, euskararatzea...
*   Proiektu propioak
*   ...
*   Zurea?
</div>



#### Interesaturik?

**Ostiralero**, **17:00**tatik **20:00**tara [tailer irekia](http://txondorra.net/tailerrak/software-eta-hardware-libre-tailerra/) ospatzen dugu, gerturatu eta elkar ezagutuko dugu!
