---
title: Zerbitzu Kolektiboak
id: 82
date: 2015-05-02 15:59:34
---

## Webgunea

Zure ekimenaren webgunea online izateko aukera. WordPress gunea sortzen lagundu zaitzakegu!

## Streaming

[Icecast2](http://icecast.org/) zerbitzaria dugu martxan ([http://zerb01.hackmildegia.net:8000](http://zerb01.hackmildegia.net:8000)), eskatu zure _endpoint_-a!

## XMPP

XMPP zerbitzaria dugu martxan (sarea.hackmildegia.net), lasai oso erabili dezakezu(te). Hori bai, gure zertifikatuaz fidatu beharko zerate oraingoz... Kontuak automatikoki sortzen dira XMPP bezero batekin lehen aldiz konektatzean.

*   [Pidgin](https://pidgin.im/)
*   ...