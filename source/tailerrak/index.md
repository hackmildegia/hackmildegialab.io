---
title: Tailerrak
id: 30
date: 2014-08-25 16:48:52
---

# Ostiraleroko tailer irekia

**Ostiralero, 17:00-20:00 tartean tailer irekia ospatzen dugu**. Ez dugu norabide zehatzik jarraitzen, momentuan erabaki/egiten dugu, edo lasai hitzegin… Inork ordenagailurik ekartzen badu GNU/Linux zaporeren bat instalatuko dugu, ala arazoak konpontzen saiatu… Arduinoei hautsa kendu eta martxan jarri, softwareren bat aztertu…

Lasai eta gustura egoteko gunea da ostiraleroko tailerra. Gerturatu!