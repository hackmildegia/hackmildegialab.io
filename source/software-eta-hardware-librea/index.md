---
title: Software eta Hardware Librea
id: 16
date: 2014-08-25 16:34:28
---

[[wikipediatik](https://eu.wikipedia.org/wiki/Software_libre)]** Software librea** ([ingelesez](https://eu.wikipedia.org/wiki/Ingeles "Ingeles"), _free software_), eskuratu ondoren erabili, kopiatu, aztertu, moldatu eta banatu daitekeen [softwarea](https://eu.wikipedia.org/wiki/Software "Software") da. [Internet](https://eu.wikipedia.org/wiki/Internet "Internet") bidez doan edo beste medio batzuetan banaketa ordainduz eskuratu ahal izaten da. Dena dela, azken hau ez da derrigorrezkoa, eta libre egoera hori mantenduz ere, saldu egin daiteke. Era berean, doako softwarea ([freeware](https://eu.wikipedia.org/w/index.php?title=Freeware&amp;action=edit&amp;redlink=1 "Freeware (sortu gabe)")) ere [iturri-kodearekin](https://eu.wikipedia.org/wiki/Iturri-kode "Iturri-kode") batera banatzen da batzuetan. Baina _freeware_ ez da software librearen berdina, moldatzeko eta banatzeko aukera eskaintzen ez den kasuetan bederen.

[Softwarea](https://eu.wikipedia.org/wiki/Software "Software") librea dela esan ohi da hurrengo **lau eskubide** hauek betetzen dituenean:

*   **helburua edozein dela ere exekutatu ahal izatea** (pribatua, hezkuntza, publikoa, komertziala, eta abar).
*   **aztertu eta aldatu ahal izatea** (horretarako beharrezkoa da kodea eskuratu ahal izatea).
*   **kopiatu ahal izatea.**
*   **hobetu, eta hobekuntza horiek publiko egin ahal izatea**, komunitatearen onurarako.
Software librea dohain egoten da eskuragarri, edo bestela banaketa kostuaren prezioan; hala ere, hau ez da derrigorrezkoa. Software librea ez da doako softwarearekin (_freeware_) nahasi behar; izan ere, software librea bide komertzialak erabiliz ere bana daiteke (_merkatuko softwarea_ edo _software komertziala_). Bide beretik, dohaineko softwareak batzuetan iturri kodea dakar baina ez beti, beraz, software hau ez da askea programaren bertsio eraldatuen moldaketa eta birbanaketa eskubideak ez badira bermatzen.