---
title: Azpiegitura
id: 14
date: 2014-08-25 16:33:27
---

Zerotik hasia eta **dohaintzetan oinarritua** da **HACKmildegia**

<div class="bg-info" style="padding:10px;margin-bottom:10px;">

#### Azpiegitura xumea dugu oraingoz:

*   Internet konexioa [Ttan Ttakun irratiak](http://ttanttakun.org) eskeintzen digu. HACKmildegiak, aldi berean, [Txondorra](http://txondorra.net)ren wifi sarea kudeatzen du.
*   Sare kable eta switch pare bat
*   **Arduino** pare bat eta hainbat konponente frogatxoak egiteko
*   Edonork erabiltzeko zaharberritutako ordenagailu bat
*   Konpondu/moldatu/frogatu behar diren hainbat ordenagailu, inpresora eta gailu
</div>

<div class="bg-success" style="padding:10px;margin-bottom:10px;">

#### Erabiltzen ez duzun gailuren bat al duzu?

Dohaintzak dira oraingoz material _berria_ lortzeko dugun bide bakarra. **[Informazio gehio hemen](http://hackmildegia.net/dohaintzak/ "Dohaintzak")**.

</div>