---
title: 'hm-ddns.sh: DNS dinamikoak eguneratu'
tags:
  - domain
  - dynamic
  - DynDNS
  - github
  - hm-ddns
  - hm-ddns.sh
  - ip
  - OVH
categories:
  - Proiektua
date: 2015-03-06 12:19:07
---

[Lehen proiektutxoa askatu degu](https://github.com/hackmildegia/hm-ddns.sh), **DNS dinamikoak eguneratzeko _script_ txiki bat** da. **Ttan Ttakun** irratiarentzako sortua izan da, **OVH** erabiltzen dute domeinuak kudeatzeko eta **dyndns2** zerbitzua/protokoloa eskeintzen dute domeinuen DNSak dinamikoki ezartzeko (http://guide.ovh.com/DynDns). Dena den, beraiek eskeintzen dituzten bideen birtartez ez genuen lortzen eta ikerketa pixka bat eginda gure propioa sortu degu! Hona hemen ba **hm-ddn.sh**!

https://github.com/hackmildegia/hm-ddns.sh
