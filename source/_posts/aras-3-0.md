---
title: ARAS 3.0
tags:
  - ARAS
  - gnu/linux
  - irratia
  - radio
  - software
  - ttanttakun
categories:
  - Berriak
date: 2015-05-08 11:22:32
---

![aras3](http://hackmildegia.net/wp-content/uploads/2015/05/aras3-300x300.png)
aras-player (goian) eta aras-recorder (behean)

Vigoko **Erasmo**k [[@erasmo1982](https://twitter.com/erasmo1982)] [ARAS](http://aras.sourceforge.net/) **irrati automatizazio sistema**ren eguneraketa plazaratu berri du, **3.0 bertsioa** dagoeneko. Hainbat fix, hobekuntza eta interfaze oso txukuna gehitu dizkio.

[Ttan Ttakun irratian](http://ttanttakun.org) duela 5 urte hasi ginen ARAS erabiltzen eta ordundik arazorik gabe erabili izan dugula esan beharra dago. **Oso zurrun eta trinkoa da**: esaten duena egiten du eta kitto. [GStreamer](http://gstreamer.freedesktop.org/) darabil multimedia prozesatzeko eta hainbat irteera erabili ditzake: _pulsesink_, _alsasink_, _osssink_, _oss4sink_, _jackaudiosink_, _filesink_. _Ordu-seinalea_ du, _interval_signal_, abestien arteko _fade_-a... [Hemen ezaugarriak](http://aras.sourceforge.net/features.html).

Ttan Ttakun-en [jack](http://jackaudio.org/) erabiltzen dugu eta honela irteera prozesatu dezakegu [CALF](http://calf.sourceforge.net/) edo beste software/pluginekin. [ARAS-en webgunean](http://aras.sourceforge.net) hainbat aukera azaltzen dizkigu:

*   _jack_-ekin jolastea (gu [jack.plumbing](http://rd.slavepianos.org/?t=rju)-en bidean gabiltza, jack zerbitzaria [OpenVZ](https://openvz.org/Main_Page) / [lxc](https://openvz.org/Main_Page) edukiontzi batean nahi dugulako, baina <sup>hori <sup>beste kontu <sup>bat da</sup></sup></sup>)
*   zuzenean [Icecast](http://icecast.org/) jario bezala emititzea
*   ARAS-i musika ordez bideoak ornitzea... **TELEBISTA BAT KUDEATU/PROGRAMATZEKO!** GStreamer erebiltzen duenen multimedia prozesatzeko ez du arazorik bata edo bestea irakurtzen! Gainera pantaila/kanal bat baino gehiago kudeatu daitezke, **hau aztertu beharra dago!**
Hiru programatxo dira ARAS _suite_-a:

*   **aras-player**: hau da irratia 24 orduz emititzen duen programatxoa! [GTK+](http://www.gtk.org/) interfazea du.
*   **aras-daemon**: aurrekoa bezala, baina interfazerik gabe.
*   **aras-recorder**: grabatzeko erremintatxoa.
Hiru testu fitxategiren bidez kudeatzen da ([dokumentazioa](http://aras.sourceforge.net/documentation.html)):

*   **aras.conf**: hobespenak, egunerokotasunean ez da aldatzen
*   **aras.block**: _blokeak_ definitzen dira hemen. _Blokeak_ tarteak lirateke: musika tarteak, errepikapenak, zuzeneko streamak... Hauek dira bloke motak:

    *   **File**: fitxategi bat erreproduzituko du ARAS-ek. _URI_ bat behar du izan, lokala (`file://`) ala internetekoa (`http://`)! Irratiarekin kalean gaudenean stream bat sortzen dugu eta ARAS programatzen dugu ordu batean erreproduzitzen hasi eta beste batean amaitzeko. Honela egiten dugu zuzenean kaletik, oso erosoa da.

      `errepikapena File file:///home/radio/archive/my_program.ogg`
      `ttanttakun_zuzenean File http://ttanttakun.org/stream.ogg`

    *   **Playlist**: **m3u** zerrenda bat erreproduzituko du ARAS-ek.

      `rock_zerrenda Playlist /home/radio/archive/rock.m3u`

    *   **Random**: karpeta bateko fitxategiak ausaz erreproduziko ditu.

      `reggae_musika Random /home/musika/reggae/`

    *   **Randomfile**: karpeta bateko fitxategi **bat** ausaz erreproduzituko du.

      `reggae_kuñak Randomfile /home/kuñak/reggae/`

    *   **Interleave**: definitutako blokeak erreproduzituko ditu zzenbaki parametro batzuen baitan.

      `reggae_tartea Interleave "reggae_musika reggae_kuñak 4 1"`

`reggae_tartea`-k zera egingo du: 4 abesti erreproduzitu `reggae_musika` bloketik eta ondoren kuña bat `reggae_kuñak` bloketik.

*   **aras.schedule**: **asteko** ordutegia definitzen da hemen. Oso sinplea da baita ere. Formatua honakoa du: eguna (astegunak ingeleraz), ordua, eta blokea.

    `Monday 22:00:00 reggae_tartea`
    `Friday 00:03:03 ttanttakun_zuzenean`

Hau da dena, ez dago misterio gehiagorik! Ikusten duzue **ARAS zurrun, lerden, trinko, erraz, sinplea**... dela!

## Erabili/Frogatu

Fitxategiak [hemen](http://sourceforge.net/projects/aras/files/aras/ARAS-3.0/) topatuko dituzue, **.deb**-ean paketatuta dator, baina oso erraz konpilatu daiteke.

## ARAS + RasFM

Ttan Ttakun irratian ARAS-i _soineko_ bat sortu diogu ordutegia eta abar web bidez kudeatu ahal izateko.


![ttnFM](/media/2015/05/proto_rasfm.png)


![ARAS ordutegia webgunean](http://hackmildegia.net/wp-content/uploads/2015/05/ttn_web_ordutegia_player.png)


Urtetako lan, froga, prototipo... hauek dagoeneko osotzen ari den [RasFM](http://rasfm.baibalab.net/) proiektuaren oinarri dira. **RasFM-ren bihotza ARAS izango da!**.

**Adi beraz berri eta eguneraketa berrieri!**

PD: **Eskerrikasko Erasmo!!**

<span title="Egilea" class="glyphicon glyphicon-pencil"></span> **[jimakker](https://twitter.com/jimakker)**
