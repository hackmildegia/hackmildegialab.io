---
title: ASUS Chromebox bat GNU/Linuxbox bilakatzeko gida
date: 2016-03-24 20:11:45
author: jimakker
categories:
  - hack
  - hardware
  - GNU/Linux
  - Gida
tags:
  - ASUS
  - Chromebox
  - ChromeOS
  - coreboot
  - SeaBIOS
  - Xubuntu
  - GNU/Linux
  - Ttan Ttakun
---

ASUS Chromebox-ak prezio interesgarrian ([230€ inguru](https://www.amazon.es/ASUS-CHROMEBOX-M118U-Ordenador-sobremesa-DisplayPort/dp/B00R0ZT1ZS)) erosi daitezken makina interesgarriak dira. Eguneroko erabilerarako (web-a nabigatzea, bideoak ikustea...) arras boteretsuak, [media-center bezala](http://kodi.wiki/view/Chromebox) erabili ohi dira adibidez. Hori bai, 16GB-eko SSD diska gogorrarekin dator, dena Google-ren *lainoan* gertatuko baita. Espazio gehiago duen batekin alda dezakegu, baina sistema eragilea eta hainbat programa instalatzeko nahikoa da eta datuak USB diska gogor batean gorde daitezke. Irratiaren kasuan, erabiltzaileen `/home`-ak [FreeNAS](http://www.freenas.org/) zerbitzarian daudenez, ez dugu arazorik ;)

Kontuak kontu, Ttan Ttakun irratian erabiltzen ditugun ordenagailuak hauekin ordeztu/ko ditugu, guztira hiru izango dira (oraingoz!). Hiruretan software bera, konfigurazio bera... Plana [Xubuntu](http://xubuntu.org/) tuneatu bat prestatzea da, behar den guztiarekin, eta ahal dela, laugarren chromebox bat erreserban, ordenagailuekin arazoren bat suertatzean soluzioa *plug&play* izan dadin!

ASUS Chromebox-ak ChromeOS sistema eragilea dakar instalatuta. Dena Google-ren bitartekaritzaren bidez gertatzen da eta guri hau ez zaigu batere interesatzen. Baina... [coreboot/SeaBIOS](https://www.coreboot.org/SeaBIOS) flasheatu diezaiokegu eta honela edozein GNU/Linux banaketa instalatu! Erraza da baina informazio nahasi xamarra topatu daiteke Interneten, goazen ba prozesu eguneratua pausuz pausu azaltzera!

(irudi kaxkar xamarrak dira, baina besterik ezean...)

Behar dugun materiala:
![materiala](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/01.jpg)

Hau da gailua:

![ASUS Chromebox](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/02.jpg)
Polita ezta!

## Garatzaile modua aktibatu

1. RESET botoia sakatuta dugula piztu gailua
![piztu gailua RESET botoia sakatuta duzularik](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/04.jpg)

2. Pantaila honek arazo baten berri emango digu, ondo, hau da nahi deguna
![yeah yeah ok](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/05.jpg)
3. `Ctrl` + `d` sakatu
![thx](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/06.jpg)
3. Klikatu berriz RESET botoia. Gailua berrabiaraziko da.
4. Itxaron...
![waiting...](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/07.jpg)
![omg](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/08.jpg)
![wat](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/09.jpg)

Behin ChromeOS martxan dagoela...
![kaixo eta agur](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/10.jpg)

1. Sakatu `Ctrl` + `Alt` + `F2` dena batera
![potato quality](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/11.jpg)
2. `chronos` erabiltzaile bezela logeatu. Hau da, sakatu `chronos` eta `ENTER`
3. Superadmin bilakatu: `sudo bash`
![sudo make me a sandwich](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/12.jpg)
4. Garatzaile modua aktibatu: `chromeos-firmwareupdate --mode=todev`
![todev](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/13.jpg)

## Coreboot + SeaBIOS

Orain **coreboot** flasheatuko diogu. Bada firmwarea ikutu gabe [Ubuntu instalatzeko/exekutatzeko aukerarik](https://github.com/dnschneid/crouton) baina gailua askatu nahi dugunez firmwarea gainidazteko segurtasun switch (torloju!) bat kendu behar diogu. Guazen ba:

1. Gomatxoak kendu eta gorde ixkin batean
![gomatxoak kentzen](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/14.jpg)
2. Torlojuak askatu
 ![torlojuak askatu](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/15.jpg)
3. Pixkat laguntzea komenigarria da
![saiatu ez izorratzen](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/16.jpg)
4. Hau da kendu behar den torlojua
![torlojua](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/17.jpg)
![kentzen](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/18.jpg)
5. Dena berriro muntatu

Gailua piztuko dugu orain. Pixka bat itxaron eta ChromeOS sarrera pantailara iritsiko gera.

1. Sakatu `Ctrl` + `Alt` + `F2`, dena batera eta `chronos` erabiltzaile bezala logeatu.

2. [Script hauek](https://github.com/MattDevo/scripts) erabiliko ditugu orain, `setup-firmware.sh` interesatzen zaigu batez ere, script honen bidez coreboot flasheatuko dugu eta. Exekutatu honakoa (adi, **O** letra larria da, ez zero):

  `cd; curl -L -O https://goo.gl/1hFfO3; sudo bash 1hFfO3`

  ![menua](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/19.jpg)

3. Sakatu `5` eta `ENTER`.
  ![coreboot deskargatzen](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/20.jpg)
4. Zure gailua zerrendan dagoela segurtatu eta `y` + `ENTER` sakatu
5. Darabilkien firmwarearearen *bakcup*-a egitea eskeiniko dizu, gure kasuan arazoak izan ditu pausu honetan eta azkenean backup gabe jarraitu dugu inolako arazorik gabe. Backup-a USB-an gordetzean izan dugu arazoa. Komandoak eskuz exekutatu ahal genituen baina... ez dugu egin. Dena den, bada ez bada ere, backup-a egin!

  ![from lost to the river eta dena ondo](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/21.jpg)
6. Menura itzuli (`ENTER`) eta `6` + `ENTER` sakatu gailua berrabiarazteko

Pantaila honek emango digu ongietorria:

![SeaBIOS](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/22.jpg)

Dena ondo joan da! Diska gogorra hutsik dugu eta horregatik ez da ezer ere ez abiarazten.

Azken pausuak:
1. Gustoko GNU/Linux **live-usb** banaketa bat (64bit) prestatu
2. Gailuan txertatu
3. Piztu
4. `ESC` botoia sarritan sakatu abiarazte menua (Boot Menu) azaltzeko:
![Boot Menu](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/23.jpg)
5. Usb gailua aukeratu (`2` gure kasuan) eta...
![profit!](/media/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/24.jpg)

Arazorik gabe frogatu eta instalatzeko aukera :)
