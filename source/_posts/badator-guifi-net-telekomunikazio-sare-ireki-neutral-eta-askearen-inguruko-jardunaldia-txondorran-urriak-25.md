---
title: 'Badator guifi.net! Telekomunikazio sare ireki, neutral eta askearen inguruko jardunaldia Txondorran, Urriak 25'
tags:
  - andoian
  - donostia
  - GISA
  - guifinet
  - hernani
  - jardunaldia
  - kolektiboa
  - orereta
  - pasaia
  - sarea
  - telekomunikazioa
  - txondorra
categories:
  - Berriak
  - Proiektua
date: 2015-10-15 11:32:50
---

**Berri gozo-gozoekin gatoz! **
[Guifi.net](https://guifi.net/) sarea inguruetan zabaltzeko ekimenaren parte hartzeko aukera izango du [Txondorrak](http://txondorra.net), eta gauzak **antolatu** eta auzoari zein interesa duen edonori **informazioa zabaltzeko jardunaldia** antolatu degu **Urriak 25**erako (igandea) eguerdiko 12:00tan.

[GISA](http://www.gisa-elkartea.org) elkartekoek aurkezpen hau prestatu dute

Gaur egun **egunero konektatzen gara Internetera**, gure eguneroko bizitzan errotuta dagoen ohitura bat bihurtu da. Baina gure **komunikazio guztiak telekomunikazio enpresen esku daude**, ezin gara gure artean konektatu enpresa bati ordaindu gabe, eta beraiek eskaintzen dizkiguten zerbitzuen menpe gaude.

Gure helburua **sare herritar bat** osatzea da, **pertsonak beraien artean konektatzeko**. sare honetara edozein konekta daiteke, enpresa bat kontratatzeko beharrik gabe. internetera ateratzeko ordaindu behar bada ere (gaur egun internetera konektatzeko modu bakarra baita), konexio bat etxe askorekin parteka daiteke, gastuak murriztuz, edo etxeko musika urrunetik atzitu daiteke adibidez. ideia gehiago nahi badituzu bilerara hurbildu zaitez :)

**GISA** elkartea irabazi asmorik gabeko elkartea da, bere helburua **software librea bultzatzea** da, eta orain **guifi.net proiektua aurkeztera dator**, Kaputxinoseko gure bulegoko konexioa partekatzeko asmoz.

Informazio gehio zabalduko degu laister, adi!

Hemen Guifi.net-en azalpen bizkorra:

![GUIFINET_slide2](/media/2015/10/GUIFINET_slide2-1024x724.png)

![GUIFINET_slide1](/media/2015/10/GUIFINET_slide1-1024x724.png)

Ikusi mapan zein interesgarria den proiektua:

![GUIFINET_mapa1](/media/2015/10/GUIFINET_mapa1-1024x724.png)
![GUIFINET_mapa2](/media/2015/10/GUIFINET_mapa2-1024x724.png)
