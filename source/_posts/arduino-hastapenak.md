---
title: ARDUINO Hastapenak
tags:
  - arduino
  - ekaina
  - hastapenak
  - ikastaroa
  - maiatza
categories:
  - Tailerrak
date: 2015-05-02 15:28:15
---

[@jimakker](https://twitter.com/jimakker)-ek eskeiniko digu itzelezko ikastaroa:

> Teknologia tailer berri eta interesgarria izango dugu gurean [**HACKmildegia**](http://hackmildegia.net)ri esker: **ARDUINO Hastapenak**. Oraingoz bi txanda, Maiatza eta Ekaina.>
> <div style="background:#eee;;font-size:large;color:#333;">>
> [![arduino_hastapenak_1024](http://txondorra.net/wp-content/uploads/2015/04/arduino_hastapenak_1024-212x300.png)](http://txondorra.net/wp-content/uploads/2015/04/arduino_hastapenak_1024.png)**[ARDUINO](http://arduino.cc "Arduino webgunea") Hastapenak** ikastaroan 4 egunetan beste hainbeste proiektu txikiren bitartez software eta hardware librearen munduan murgilduko gara.>
>
> Lau larunbatetako txandatan ARDUINOrekin jolastu eta ikasi egingo dugu, **aurre esperientziarik behar ez duten proiektutxoekin**. Bakoitzak bere ordenagailu eramangarria eta ARDUINOa ekartzea komenigarria da baina ez da beharrezkoa!>
>
> Txandak:>
> **MAIATZA**: 9, 16, 23, 30>
> **EKAINA**: 6, 13, 20, 27>
> </div>>
> Informazio guztia eta izenematea: [http://txondorra.net/tailerrak/arduino-hastapenak](http://txondorra.net/tailerrak/arduino-hastapenak/)
