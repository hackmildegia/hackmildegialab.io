---
title: Tailer irekia ostiralero
tags:
  - hardware
  - irekia
  - software
  - tailer
  - txondorra
categories:
  - Berriak
  - Tailerrak
date: 2015-01-12 11:03:53
---

**Ostiralero, 17:00-20:00** tartean **[tailer irekia](http://txondorra.net/tailerrak/software-eta-hardware-libre-tailerra/)** ospatzen dugu. Ez dugu norabide zehatzik jarraitzen, momentuan erabaki/egiten dugu, edo lasai hitzegin... Inork ordenagailurik ekartzen badu GNU/Linux zaporeren bat instalatuko dugu, ala arazoak konpontzen saiatu... Arduinoei hautsa kendu eta martxan jarri, softwareren bat aztertu...

Lasai eta gustura egoteko gunea da ostiraleroko tailerra. Gerturatu!
