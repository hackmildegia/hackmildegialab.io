---
title: Irudiak multzoan prestatu
author: jimakker
tags:
  - imagemagick
  - exiv2
  - exif
categories:
  - Software
  - Gida
date: 2016-03-28 11:30:00
---
[Aurreko gida prestatzeko](http://hackmildegia.net/2016/03/24/asus-chromebox-bat-gnu-linuxbox-bilakatzeko-gida/) mugikorrarekin ateratako hainbat argazki txikitu eta garbitu behar nituen, hogei bat guztira. Eskuz egiteko gehiegitxo!

Badira interfazea (GUI) duten hainbat programa, baina erarik sinpleena terminaletik bertatik exekutatzen diren bi programatxorekin da.

## Irudiak txikitu
Kamera digitalekin ateratzen diren argazkiak poster tamainakoak dira (gutxienez), web-ean erabiltzeko txikitu egin behar ditugu. Horretarako [imagemagick](http://imagemagick.org/script/index.php)-ek dakarren `mogrify` komandoa erabiliko degu:

Instalatu imagemagick:

`sudo apt-get install imagemagick`

Irudiak dauden karpetara joan eta mogrify exekutatu:

`cd /irudiak/karpeta`

`mogrify -resize 1024x768 *.jpg`

Karpetako `jpg` irudi guztiak gehieneko `1024x768` erresoluziora txikituko ditu, itxura ratioa (aspect ratio) mantenduz!

`mogrify` komandoak funtzionaltasun ugari ditu, manuala irakurtzeko exekutatu: `man mogrify`.

## Irudiak garbitu (EXIF informazioa ezabatu)
Kamera digitalek EXIF informazioa gehitzen diete argazkiei. Argazkia atera den uneko hainbat metadatu gordetzen dira eta informazio pertsonalaren isurketa (leak) bat izan daiteke. Ohitura ona da, beraz, datu hauek, argazkia Internet-era igo baino lehen, ezabatzea. [exiv2](http://www.exiv2.org/) izena duen programatxoa erabiliko dugu:

`sudo apt-get install exiv2`

Irudiak dauden karpetara joan eta exiv2 exekutatu:

`cd /irudiak/karpeta`

`exiv2 rm *.jpg`

`jpg` irudi guztiei ezabatuko dizkie datuak.

Eta kitto! Hamaika irudi erraldoi txikitu eta garbitu ditugu bi pausu sinpletan. Orain Internet-era igotzea besterik ez da gelditzen!
