# hackmildegia.net

[hackmildegia.net](http://hackmildegia.net) HACKmildearen webgunea/bloga.

Artikuluren bat idatzi nahi baduzu:
- **forkeatu** proiektua
- artikulua idatzi
- **Merge Request** bat bidali.

Edozein hizkuntzatan idatzitako teknologia askeen inguruko artikulu orijinalak izan behar dute. Kolektibo editorialean parte hartzeko aukera dago baita ere.

## Contributing
- Fork project
- Write post
- Send *Merge Request*
